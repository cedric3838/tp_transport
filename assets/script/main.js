jQuery('document').ready(function($) {

    // map modal

    let mymap = L.map('mapid').setView([45.1667, 5.7167], 10);

    L.tileLayer('https://api.mapbox.com/styles/v1/{id}/tiles/{z}/{x}/{y}?access_token={accessToken}', {
        attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors, <a href="https://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, Imagery :copyright: <a href="https://www.mapbox.com/">Mapbox</a>',
        maxZoom: 18,
        id: 'mapbox/streets-v11',
        tileSize: 512,
        zoomOffset: -1,
        accessToken: 'pk.eyJ1IjoibWlja2FlbHYiLCJhIjoiY2tlZHp5ZHpnMGI2NzJ6b3lneXkyMmlnZSJ9.qw_xV6G2HNJliXo2eaTjKw'
    }).addTo(mymap);

    // les variables

    let resTram = document.getElementById('resTram');
    let resC38 = document.getElementById('resC38');
    let resNav = document.getElementById('resNav');
    let resSnc = document.getElementById('resSnc');
    let resChrono = document.getElementById('resChrono');
    let resFlexo = document.getElementById('resFlexo');
    let res_Scol = document.getElementById('resScol');
    let resUbr = document.getElementById('resUrb');
    let resInter = document.getElementById('resInter');
    let resTad = document.getElementById('resTad');

    let inscolaire = document.getElementById('scolaire');
    let intram = document.getElementById('tramway');
    let innavette = document.getElementById('navette');
    let inproxi = document.getElementById('proxi');
    let inflexo = document.getElementById('flexo');
    let inchrono = document.getElementById('chrono');
    let insnc = document.getElementById('snc');
    let inc38 = document.getElementById('c38');
    let inurb = document.getElementById('urbaines');
    let inInter = document.getElementById('interurbaines');
    let inTad = document.getElementById('tad');

    let aller = document.getElementById('aller');
    let retour = document.getElementById('retour');
    let longtxt = document.getElementById('longname');
    let shortxt = document.getElementById('shortname')
    const nextHours = document.getElementById('#nextHours')

    let icon = document.querySelectorAll('.spanbtn')

    let url_lignes = "http://data.metromobilite.fr/api/routers/default/index/routes";

    // la map modale

    // accodeon pour liste de lignes

    var acc = document.getElementsByClassName("accordion");
    var i;

    for (i = 0; i < acc.length; i++) {
        acc[i].addEventListener("click", function() {
            this.classList.toggle("active");
            var panel = this.nextElementSibling;
            if (panel.style.display === "block") {
                panel.style.display = "none";
            } else {
                panel.style.display = "block";
            }
        });
    }

    // boucle pour le Json



    function resList(data) {

        const tabIds = data.map(i => i.id);
        // console.log(tabIds);

        const tabC38 = data.filter(i => i.type === "C38");
        // console.log(tabC38);
        const tabFlexo = data.filter(i => i.type === "FLEXO");
        // console.log(tabFlexo);
        const tabScol = data.filter(i => i.type === "SCOL");
        // console.log(tabScol);
        const tabChrono = data.filter(i => i.type === "CHRONO");
        // console.log(tabChrono);
        const tabNav = data.filter(i => i.mode === "BUS" && i.type === "TRAM");
        const colNav = data.filter(i => i.color);
        // console.log(colNav)
        // console.log(tabNav);
        const tabTram = data.filter(i => i.mode === "TRAM" && i.type === "TRAM");
        // console.log(tabTram);
        const tabRail = data.filter(i => i.type === "CHRONO");
        // console.log(tabRail);
        // Pays voironnais
        const tabTad = data.filter(i => i.type === "TAD");
        // console.log(tabTad);
        // ToGO
        const tabStruc = data.filter(i => i.type === "Structurantes");
        // console.log(tabStruc);
        // ToGO
        const tabSec = data.filter(i => i.type === "Secondaires");
        // console.log(tabSec);
        // Pays voironnais
        const tabUrb = data.filter(i => i.type === "Urbaines");
        // console.log(tabUrb);
        const tabInt = data.filter(i => i.type === "Interurbaines");
        // console.log(tabInt);
        // SNCF
        const tabSNC = data.filter(i => i.type === "SNC");
        // console.log(tabSNC);

        const ctnmodal = Swal.mixin({
            customClass: {

                html: '<ul></ul>'

            },
            buttonsStyling: false
        })


        $('#scolaire').one('click', function() {

            for (const scol of tabScol) {
                let listSco = $('<span></span>'); // Create a <button> element
                listSco.text(scol.shortName);
                listSco.addClass('spanbtn');
                listSco.css("background-color", "#" + scol.color);
                listSco.attr('data-id', scol.id)
                listSco.attr('data-longName', scol.longName);
                listSco.attr('data-shortName', scol.shortName); // Insert text
                // listSco.addClass('in-content');
                $("#resScol").append(listSco);

                $.ajax({

                    // Adresse à laquelle la requête est envoyée
                    url: `http://data.metromobilite.fr/api/ficheHoraires/json?route=` + scol.id,

                    // La fonction à apeller si la requête aboutie
                    success: function(datascol) {
                        // Je charge les données dans box
                        const arscaller = datascol[0].arrets[i];
                        // console.log(arNav);

                        if (datascol[0].arrets[i]) {

                            $('.spanbtn').one('click', function() {

                                longtxt.innerText = listSco.data('longname');
                                // let allersco = $('<li></li>');
                                // allersco.text('YEAT');
                                let tta = document.createElement('li');
                                $(tta).text(datascol[0].arrets[i].stopName)
                                aller.append(tta);


                                let aat = document.createElement('li')
                                $(aat).text(datascol[1].arrets[i].stopName);
                                retour.append(aat);

                                document.getElementById('id01').style.display = 'block'

                                document.getElementsByClassName("tablink")[0].click();

                                this.onclick

                                function openCity(evt, cityName) {
                                    var i, x, tablinks;
                                    x = document.getElementsByClassName("spanbtn");
                                    for (i = 0; i < x.length; i++) {
                                        x[i].style.display = "none";
                                    }
                                    tablinks = document.getElementsByClassName("tablink");
                                    for (i = 0; i < x.length; i++) {
                                        tablinks[i].classList.remove("w3-light-grey");
                                    }
                                    document.getElementById(cityName).style.display = "block";
                                    evt.currentTarget.classList.add("w3-light-grey");
                                }
                                let timestamp = Math.round(new Date().getTime());
                                let url3 = `http://data.metromobilite.fr/api/ficheHoraires/json?route=` + scol.id + `&
                                time=` + timestamp;
                                // console.log(url3);
                                console.log(scol.id);
                                $.ajax({
                                    url: url3,


                                    success: function(data1) {

                                        function afficherHoraires(data1) {
                                            console.log(data1)

                                            for (let j = 0; j < data1[0].arrets.length; j++) {
                                                let nomArrets = data1[0].arrets[j].stopName;
                                                // console.log(nomArrets)


                                                let next = data1[0].arrets[j].trips[0];
                                                // console.log(next);
                                                let id_id = data1.id
                                                    // console.log(listSco.data('id'))

                                                // console.log(secondsToHms(next))



                                                let li = "<li>" + nomArrets + "Prochain passage : " + secondsToHms(next) + "</li>";
                                                $('#nextHours').append(li)

                                            }
                                        }

                                        // console.log(data1);
                                        // console.log(url);
                                        afficherHoraires(data1);
                                        afficherHorairesInverse(data1);
                                    },

                                })
                            });

                        };

                        // arNav.innerHTML('li')
                        // ul.appendChild(arNav),
                        // console.log(arNav);
                    },

                    // La fonction à appeler si la requête n'a pas abouti
                    error: function() {
                        // J'affiche un message d'erreur
                        console.log("Désolé, aucun résultat trouvé.");
                    }

                }).done(function() {
                    console.log
                })

            }
        });

    };

    $('#c38').one('click', function() {

        for (const c_38 of tabC38) {
            let listc38 = $('<span></span>'); // Create a <button> element
            listc38.text(c_38.shortName);
            listc38.addClass('spanbtn');
            listc38.css("background-color", "#" + c_38.color);
            listc38.attr('id', listc38.id);
            listc38.attr('id', c_38.longName);
            listc38.attr('id', c_38.shortName); // Insert text
            // listc38.addClass('in-content');
            $("#resC38").append(listc38);
            $('.spanbtn').click(function() {

                Swal.fire({
                    title: (c_38.longName),
                    text: (c_38.shortName),
                    imageHeight: 1500,
                    imageAlt: 'Déscription de la ligne',
                    allowOutsideClick: 'false',
                    allowEscapeKey: 'true',
                });

            });


            $.ajax({

                // Adresse à laquelle la requête est envoyée
                url: `http://data.metromobilite.fr/api/ficheHoraires/json?route=` + c_38.id,


                // La fonction à apeller si la requête aboutie
                success: function(data2) {

                    // Je charge les données dans box
                    console.log(data2)
                },

                // La fonction à appeler si la requête n'a pas abouti
                error: function() {
                    // J'affiche un message d'erreur
                    console.log("Désolé, aucun résultat trouvé.");
                }

            })
        }

    });

    $('#flexo').one('click', function() {

        for (const flexo of tabFlexo) {
            let listflexo = $('<span></span>'); // Create a <button> element
            listflexo.text(flexo.shortName);
            listflexo.addClass('spanbtn');
            listflexo.css("background-color", "#" + flexo.color);
            listflexo.attr('id', listflexo.id); // Insert text
            // listflexo.addClass('in-content');
            $("#resFlexo").append(listflexo);
            $('.spanbtn').click(function() {

                Swal.fire({
                    title: (flexo.longName),
                    text: (flexo.shortName),
                    imageHeight: 1500,
                    imageAlt: 'Déscription ligne',
                    imageHeight: 1500,
                    imageAlt: 'A tall image',
                    allowOutsideClick: 'false',
                    allowEscapeKey: 'true',
                });

            })
        }

    });

    $('#chrono').one('click', function() {

        for (const chrono of tabChrono) {
            let listchrono = $('<span></span>'); // Create a <button> element
            listchrono.text(chrono.shortName);
            listchrono.addClass('spanbtn');
            listchrono.css("background-color", "#" + chrono.color);
            listchrono.attr('id', listchrono.id)
            listchrono.attr('id', chrono.longName);
            listchrono.attr('id', chrono.shortName); // Insert text
            // listchrono.addClass('in-content');
            $("#resChrono").append(listchrono);
            $('.spanbtn').click(function() {

                Swal.fire({
                    title: (chrono.longName),
                    text: (chrono.shortName),
                    imageHeight: 1500,
                    imageAlt: 'Déscription ligne',
                    imageHeight: 1500,
                    imageAlt: 'A tall image',
                    allowOutsideClick: 'false',
                    allowEscapeKey: 'true',
                });

            })
        }

    });

    $('#navette').one('click', function() {

        for (const nav of tabNav) {
            let listnav = $('<span></span>'); // Create a <button> element
            listnav.text(nav.shortName);
            listnav.addClass('spanbtn'); // Insert text
            listnav.css("background-color", "#" + nav.color);
            listnav.attr('id', listnav.id);
            listnav.attr('id', nav.longName);
            listnav.attr('id', nav.shortName);
            // listnav.addClass('in-content');
            $("#resNav").append(listnav);
            $('.spanbtn').click(function() {

                Swal.fire({
                    title: (nav.longName),
                    text: (nav.shortName),
                    imageHeight: 1500,
                    imageAlt: 'Déscription ligne',
                    imageHeight: 1500,
                    imageAlt: 'A tall image',
                    allowOutsideClick: 'false',
                    allowEscapeKey: 'true',
                });

            })
        }

    });

    $('#snc').one('click', function() {

        for (const snc of tabSNC) {
            let listsnc = $('<span></span>'); // Create a <button> element
            listsnc.text(snc.shortName);
            listsnc.addClass('spanbtn');
            listsnc.attr('id', listsnc.id);
            listsnc.attr('id', snc.longName);
            listsnc.attr('id', snc.shortName);
            listsnc.css("background-color", "#" + snc.color); // Insert text
            // listrail.addClass('in-content');
            $("#resSnc").append(listsnc);
            $('.spanbtn').click(function() {

                Swal.fire({
                    title: (snc.longName),
                    text: (snc.shortName),
                    imageHeight: 1500,
                    imageAlt: 'Déscription ligne',
                    imageHeight: 1500,
                    imageAlt: 'A tall image',
                    allowOutsideClick: 'false',
                    allowEscapeKey: 'true',
                });

            })
        }

    });

    $('#tramway').one('click', function() {

        for (const tram of tabTram) {
            let listtram = $('<span></span>'); // Create a <button> element
            listtram.text(tram.shortName);
            listtram.addClass('spanbtn');
            listtram.css("background-color", "#" + tram.color);
            listtram.attr('id', listtram.id);
            listtram.attr('id', tram.longName);
            listtram.attr('id', tram.shortName); // Insert text
            // listtram.addClass('in-content');
            $("#resTram").append(listtram);
            $('.spanbtn').click(function() {

                Swal.fire({
                    title: (tram.longName),
                    text: (tram.shortName),
                    imageHeight: 1500,
                    imageAlt: 'Déscription ligne',
                    imageHeight: 1500,
                    imageAlt: 'A tall image',
                    allowOutsideClick: 'false',
                    allowEscapeKey: 'true',
                });

            })
        }
    });

    $('#urbaines').one('click', function() {

        for (const urb of tabUrb) {
            let listurb = $('<span></span>'); // Create a <button> element
            listurb.text(urb.shortName);
            listurb.addClass('spanbtn');
            listurb.css("background-color", "#" + urb.color);
            listurb.attr("id", urb.id);
            listurb.attr('id', urb.longName);
            listurb.attr('id', urb.shortName); // Insert text
            // listtram.addClass('in-content');
            $("#resUrb").append(listurb);
            $('.spanbtn').click(function() {

                Swal.fire({
                    title: (urb.longName),
                    text: (urb.shortName),
                    imageHeight: 1500,
                    imageAlt: 'Déscription ligne',
                    imageHeight: 1500,
                    imageAlt: 'A tall image',
                    allowOutsideClick: 'false',
                    allowEscapeKey: 'true',
                });

            })
        }
    })

    $('#interurbaines').one('click', function() {

        for (const inter of tabInt) {
            let listinter = $('<span></span>'); // Create a <button> element
            listinter.text(inter.shortName);
            listinter.addClass('spanbtn');
            listinter.css("background-color", "#" + inter.color);
            listinter.attr('id', listinter.id);
            listinter.attr('id', inter.longName);
            listinter.attr('id', inter.shortName); // Insert text
            // listtram.addClass('in-content');
            $("#resInter").append(listinter);
            $('.spanbtn').click(function() {

                Swal.fire({
                    title: (inter.longName),
                    text: (inter.shortName),
                    imageHeight: 1500,
                    imageAlt: 'Déscription ligne',
                    imageHeight: 1500,
                    imageAlt: 'A tall image',
                    allowOutsideClick: 'false',
                    allowEscapeKey: 'true',
                });

            })
        }
    })


    $('#tad').one('click', function() {

        for (const tad of tabTad) {
            let listtad = $('<span></span>'); // Create a <button> element
            listtad.text(tad.shortName);
            listtad.addClass('spanbtn');
            listtad.css("background-color", "#" + tad.color);
            listtad.attr('data-id', listtad.id);
            listtad.attr('data-longName', tad.longName);
            listtad.attr('id', tad.shortName);
            // console.log(listtad.id)                 // Insert text
            // listtram.addClass('in-content');
            $("#resTad").append(listtad);
            $('.spanbtn').click(function() {

                Swal.fire({
                    title: (tad.longName),
                    text: (tad.shortName),
                    imageHeight: 1500,
                    imageAlt: 'Déscription ligne',
                    imageHeight: 1500,
                    imageAlt: 'A tall image',
                    allowOutsideClick: 'false',
                    allowEscapeKey: 'true',
                });

            })
        }
    })


    // l'entete fix puis scroll

    var $headline = $('.headline'),
        $inner = $('.inner'),
        $nav = $('nav'),
        navHeight = 75;

    // Je définis ma requête ajax
    $.ajax({

        // Adresse à laquelle la requête est envoyée
        url: url_lignes,

        // La fonction à apeller si la requête aboutie
        success: function(data) {
            // Je charge les données dans box
            resList(data)
                // console.log(data)

            // event Tram

            // $('#choixTram').click('click', function() {


            //     divBus.innerHTML = ""
            //     divRail.innerHTML = ""
            //     divTram.innerHTML = ""

            //     let btnNav = document.createElement("BUTTON"); // Create a <button> element
            //     btnNav.innerHTML = "Navette"; // Insert text
            //     $(btnNav).attr('id', 'tarNav');
            //     divTram.appendChild(btnNav);

            //     $('#divNavette').one('click', function() {

            //         let resNav = document.createElement("li")
            //         resNav.innerText = tabNav;
            //         divTram.appendChild(resNav);
            //         // console.log(resNav)

            //     });

            //     let btn_Tram = document.createElement("BUTTON"); // Create a <button> element
            //     btn_Tram.innerHTML = "Tram"; // Insert text
            //     divTram.appendChild(btn_Tram);

            // });

            // //   event Autres

            // $('#choixRail').click('click', function() {

            //     divTram.innerHTML = ""
            //     divBus.innerHTML = ""
            //     divRail.innerHTML = ""

            //     let btnSnc = document.createElement("BUTTON"); // Create a <button> element
            //     btnSnc.innerHTML = "SNC"; // Insert text
            //     divRail.appendChild(btnSnc);


            // });

            // // event Bus

            // $('#choixBus').click('click', function() {

            //     divTram.innerHTML = ""
            //     divRail.innerHTML = ""
            //     divBus.innerHTML = ""

            //     let btnScol = document.createElement("BUTTON"); // Create a <button> element
            //     btnScol.innerHTML = "Scolaire"; // Insert text
            //     divBus.appendChild(btnScol);

            //     let btnFlex = document.createElement("BUTTON"); // Create a <button> element
            //     btnFlex.innerHTML = "FlexiBus"; // Insert text
            //     divBus.appendChild(btnFlex);

            //     let btnChrono = document.createElement("BUTTON"); // Create a <button> element
            //     btnChrono.innerHTML = "Chrono"; // Insert text
            //     divBus.appendChild(btnChrono);

            //     let btnC38 = document.createElement("BUTTON"); // Create a <button> element
            //     btnC38.innerHTML = "C38"; // Insert text
            //     divBus.appendChild(btnC38);



            // });



        },

        // La fonction à appeler si la requête n'a pas abouti
        error: function() {
            // J'affiche un message d'erreur
            // console.log("Désolé, aucun résultat trouvé.");
        }

    });

    // alert  modal

    Swal.fire({
        title: 'COVID-19!',
        text: 'Compte-tenu de la situation sanitaire et du confinement mis en place lors de la crise sanitaire du Covid-19,des mesures tarifaires exceptionnelles visent à vous remercier pour votre confiance renouvelée.',
        imageUrl: "https://www.battaglino.fr/wp-content/uploads/2014/07/Logo-Ville-de-Grenoble-Couleur-PNG--300x300.png",
        imageWidth: 100,
        imageHeight: 90,
        imageAlt: 'Custom image',
    })

    // alert bandeau

    const Toast = Swal.mixin({
        toast: true,
        position: 'bottom',
        showConfirmButton: false,
        timer: 4000,
        timerProgressBar: true,
        onOpen: (toast) => {
            toast.addEventListener('mouseenter', Swal.stopTimer)
            toast.addEventListener('mouseleave', Swal.resumeTimer)
        }
    });


    setInterval(function() {
        Toast.fire({
            icon: 'warning',
            title: "Masque et distantion sociale obligatoire sur l'esemble du réseau TAG"
        })
    }, 6000);

    // moadal carte + arrets

});